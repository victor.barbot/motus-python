import random
import fonctions_auxiliaires as aux
import re

# Couleurs


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''


class Motus:
    def __init__(self) -> None:
        self.longueur = random.randint(6, 9)
        self.dictionnaire = aux.creer_dictionnaire()
        self.liste_mots_valides = self.dictionnaire[self.longueur]
        self.mot_a_trouver = random.choice(self.liste_mots_valides)
        self.nombre_essais = 0
        self.nombre_essais_max = 6
        self.essais = []
        self.trouve = False
        self.pattern = re.compile(r"^[a-zA-Z]+$")

    def check_occurences(self, lettre, input_user):
        return aux.nombre_occurences_lettre(lettre, self.mot_a_trouver) <= aux.nombre_occurences_lettre(lettre, input_user)

    def afficher_ligne(self, input_user):
        resultat = ""

        for i in range(len(input_user)):
            lettre = input_user[i]

            # Si c'est une lettre bien placée
            if (self.mot_a_trouver[i] == lettre):
                resultat += f"{bcolors.FAIL} {lettre} {bcolors.ENDC}"

            # Si c'est une lettre mal placée
            elif (lettre in self.mot_a_trouver and self.check_occurences(self, lettre, input_user)):
                resultat += f"{bcolors.WARNING} {lettre} {bcolors.ENDC}"

            # Sinon
            else:
                resultat += f" {lettre} "

        return resultat

    def afficher_prochaine_ligne(self, input_user):
        resultat = ""

        for i in range(len(input_user)):
            if (self.mot_a_trouver[i] == input_user[i]):
                resultat += input_user[i]

            else:
                resultat += " _ "

        return resultat

    def est_une_input_valide(self, input_user):
        if not(self.pattern.match(input_user)):
            message_erreur = "N'entrer que des lettres de 'a' à 'z'"

        elif (self.mot_a_trouver[0] != input_user[0]):
            message_erreur = f"Le mot doit commencer par '{self.mot_a_trouver[0]}'"

        elif (len(input_user) != len(self.mot_a_trouver)):
            message_erreur = f"Le mot doit faire exactement {len(self.mot_a_trouver)} lettres"

        elif (input_user not in self.liste_mots_valides):
            message_erreur = f"{input_user} n'est pas un mot valide"

        else:
            message_erreur = ""

        return message_erreur

    def affichage_initial(self):
        return self.mot_a_trouver[0] + " _ " * (len(self.mot_a_trouver) - 1)

    def jeu(self):
        print(self.affichage_initial())

        while not(self.trouve) and self.nombre_essais <= self.nombre_essais_max:
            input_user = aux.nettoyer_input(input("> "))

            message_erreur = self.est_une_input_valide(input_user)

            if (message_erreur):
                print(message_erreur)
                continue

            self.nombre_essais += 1
            print(self.afficher_ligne(input_user))

            if (input_user == self.mot_a_trouver):
                self.trouve = True
                break

            print(self.afficher_prochaine_ligne(input_user))

        if self.trouve:
            nb_essais = "essais" if self.nombre_essais > 1 else "essai"

            print(
                f"Félicitations, vous avez trouvé le mot en {self.nombre_essais} {nb_essais}")

        else:
            print(
                f"Dommage, le mot à trouver était {self.mot_a_trouver.upper()}")


if __name__ == "__main__":
    motus = Motus()
    motus.jeu()
